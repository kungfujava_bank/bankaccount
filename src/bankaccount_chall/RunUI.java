package bankaccount_chall;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class RunUI {

	static Scanner scanner = new Scanner(System.in);
	
	Bank bank = new Bank();
	
		public void runUserInerface() {
			
			System.out.println("|-----------------------------------------------|");
			System.out.println("|                  Welcome To                   |");
			System.out.println("|             Kung Fu Java Banking!             |");
			System.out.println("|-----------------------------------------------|");
			
			for (BankAccount b : FileInputOutput.RetrieveFromFile("C:/DemoIo/Accounts.txt")) {
				bank.addBankAccount(b.getPassword(), b.getAccountHolder(), b.getBalance());
			};
			
			//bank.setAccounts(FileInputOutput.RetrieveFromFile("C:/DemoIo/Accounts.txt"));
			AddTransactionsToAccounts();
			System.out.println("Welcome choose a function \n"
					+ "	c - for creating new account "
					+ " v - for view existing account "
					+ " d - to make a deposit "
					+ " w - to withdraw"
					+ " t - to Transactions" 
					+ "	q - to quit the program	");

			
			boolean hasString = scanner.hasNextLine();
			while( hasString ) {
			
				
				switch(scanner.next()) {
				case "c": 
					System.out.println("c choosen");
					createAccount();
					
					break;
				case "v":
					System.out.println("v choosen");
					
					viewAccount();
					break;
				case "d":
					System.out.println("d choosen");
					makeDeposit();
				break;
				case "w":
					System.out.println("w choosen");
					withdraw();
					break;
				case "t":
					System.out.println("t choosen");
					PrintAccountsTransactions();
					break;
				case "q":
					System.out.println("q choosen, Bye");
					System.out.println("Application Exited!");
					System.out.println("All information stored in accounts and transaction files");
					FileInputOutput.flushFile("C:/DemoIO/Accounts.txt");
					FileInputOutput.PrintToFile(bank.printAcconts());
					System.out.println(bank.getAccounts().toString());
					PrintTransactions();
					hasString =false;
					scanner.close();
					break;
					default : 

						System.out.println("Option not recognised try \n"
								+ "	c - for creating new account "
								+ " v - for view existing account"
								+ " d - to make a deposit"
								+ " w - to withdraw"
								+ " t - to Transactions" 
								+ "	q - to quit the program	");
			}					
		}						
	}
		
		public void createAccount() { 
			
			Scanner scanner1 = new Scanner(System.in);
			
			System.out.println("Enter a password: ");
			int pass = 0;
			try {
				 pass = scanner1.nextInt();
//				 scanner1.nextLine();
				 
				 if(pass > 0) {
					 System.out.println("password created, enter a name");
					 String name = scanner1.next(); 
					 
					 System.out.println("Account Made, Choose an option \n"
								+ "	c - for creating new account "
								+ " v - for view existing account"
								+ " d - to make a deposit"
								+ " w - to withdraw" 
								+ " t - to Transactions" 
								+ "	q - to quit the program	");
					 bank.addBankAccount(pass, name, 0.0);
					 System.out.println("Welcome to Kung Fu Java Banking!");
					 System.out.println("Password = " + pass + "\n" + "Account Holder Name = " + name + "\n" + "Account Id = " + bank.getAccounts().get(bank.findAccount(name)).getId());
					 scanner.nextLine();
					 
					 System.out.println("Account Made, Choose an option \n"
								+ "	c - for creating new account "
								+ " v - for view existing account"
								+ " d - to make a deposit"
								+ " w - to withdraw" 
								+ " t - to Transactions" 
								+ "	q - to quit the program	");
				 }
			} catch(Exception e) {
				System.out.println("invalid input, try again ");
			}

		
		}
		
		private  void viewAccount() {
			boolean status= true;
			Scanner vScanner = new Scanner(System.in);
				
					System.out.println("Enter id : ");
					
					try {
						
							int id = vScanner.nextInt();
							System.out.println("Enter password");
							
							int password = vScanner.nextInt();
							
							System.out.println("Account Holder : " + bank.verifyAccount(id, password).getAccountHolder());
							System.out.println("Account Id : " + bank.verifyAccount(id, password).getId());
							System.out.println("Account Balance : "+ bank.verifyAccount(id, password).getBalance());
							System.out.println();
							System.out.println("Choose an option \n"
									+ "	c - for creating new account "
									+ " v - for view existing account"
									+ " d - to make a deposit"
									+ " w - to withdraw" 
									+ " t - to Transactions" 
									+ "	q - to quit the program	");
					} catch(Exception e) {
							System.out.println("Please enter a valid input, returning to menu");
							
							System.out.println("Account Made, Choose an option \n"
									+ "	c - for creating new account "
									+ " v - for view existing account"
									+ " d - to make a deposit"
									+ " w - to withdraw" 
									+ " t - to Transactions" 
									+ "	q - to quit the program	");
						}
					
		
				
				//System.out.println(bank.verifyAccount(id, password));
			
				
			}

		private  void makeDeposit() {
			
			System.out.println("ENter account number");
			try {
				Scanner scanner1 = new Scanner(System.in);
			
				int id = scanner1.nextInt();
				System.out.println("Enter amount to deposit ");
				boolean b = scanner1.hasNextDouble();
			
					if(b) {
							double amount = scanner1.nextDouble();
				
							if(amount > 0 ) {
								bank.deposit(id, amount);
								System.out.println("deposit made");
							}
				}	
			}catch (Exception e) {
				
			}
			
			System.out.println("Welcome choose a function \n"
					+ "	c - for creating new account "
					+ " v - for view existing account "
					+ " d - to make a deposit "
					+ " w - to withdraw"
					+ " t - to Transactions" 
					+ "	q - to quit the program	");
		}
		
		private void withdraw() {
			
			System.out.println("ENter account number");
			try {
				Scanner scanner1 = new Scanner(System.in);
				
				int id = scanner1.nextInt();
			
				System.out.println("Enter amount to withdraw ");
				boolean b = scanner1.hasNextDouble();
				
				if(b) {
					double amount = scanner1.nextDouble();
					
					if(amount > 0 ) {
				
						bank.withdraw(id, amount);;
						System.out.println("Withdraw made"  );
					}
				}
			}catch(Exception e) {
				System.out.println("Invlaid Input");
			}
			
			System.out.println("Welcome choose a function \n"
					+ "	c - for creating new account "
					+ " v - for view existing account "
					+ " d - to make a deposit "
					+ " w - to withdraw"
					+ " t - to Transactions" 
					+ "	q - to quit the program	");
		}
		private void PrintTransactions(){
			//Flush File so no duplicates occur
			FileInputOutput.flushFile("C:/DemoIo/TransactionsLog.txt");
			for (int i = 0; i < bank.getAccounts().size(); i++) {
				for (int j = 0; j < bank.getAccounts().get(i).getTransactions().size(); j++) {
					StringBuilder transactionsBuilder = new StringBuilder();
					//String builder to concatenate string
					transactionsBuilder.append(bank.getAccounts().get(i).getTransactions().get(j).getAccountId() + "," +
							bank.getAccounts().get(i).getTransactions().get(j).getAccountName() + "," +
							bank.getAccounts().get(i).getTransactions().get(j).getAmount() + "," + 
							bank.getAccounts().get(i).getTransactions().get(j).getType() + "\n");
					FileInputOutput.PrintTransactionsToFile(transactionsBuilder.toString());
				}
				
				
			}
		}
		private void AddTransactionsToAccounts(){
			//FileInputOutput.flushFile("C:/DemoIo/TransactionsLog.txt");
			ArrayList<Transaction> transTemp = new ArrayList<Transaction>();
			transTemp = FileInputOutput.RetrieveTransactions("C:/DemoIo/TransactionsLog.txt");
			for (int i = 0; i < transTemp.size(); i++) {
				for (int j = 0; j < bank.getAccounts().size(); j++) {
					if(transTemp.get(i).getAccountId() == bank.getAccounts().get(j).getId()){
						bank.getAccounts().get(j).getTransactions().add(transTemp.get(i));
						//System.out.println("Here" +transTemp.get(i).getAccountId() + bank.getAccounts().get(j).getId() );
					}
				}
				
			}
			
		}
		private void PrintAccountsTransactions(){
			System.out.println("Enter account number");
			Scanner scanner1 = new Scanner(System.in);
			
			int id = scanner1.nextInt();
			
			for (int i = 0; i < bank.getAccounts().size(); i++) {
				if(id == bank.getAccounts().get(i).getId()){
					System.out.println("Account Found");
					System.out.println("Account Id : " + bank.getAccounts().get(i).getId());
					System.out.println("Account Holder : " + bank.getAccounts().get(i).getAccountHolder());
					for (int j = 0; j < bank.getAccounts().get(i).getTransactions().size(); j++) {
						System.out.println("Transaction Amount : " +bank.getAccounts().get(i).getTransactions().get(j).getAmount());
						System.out.println("Transaction Type : " +bank.getAccounts().get(i).getTransactions().get(j).getType());
					}
				
				}
			}
			
			scanner.nextLine();
			
			System.out.println("Welcome choose a function \n"
					+ "	c - for creating new account "
					+ " v - for view existing account "
					+ " d - to make a deposit "
					+ " w - to withdraw"
					+ " t - to Transactions" 
					+ "	q - to quit the program	");
		}
}	

