package bankaccount_chall;

public class Transaction {
	
	private int accountId;
	private String accountName;
	private double amount;
	private String type;
	
	public Transaction(int accountId, String accountName, double amount, String type) {
		this.accountId = accountId;
		this.accountName = accountName;
		this.amount = amount;
		this.type = type;
	}

	public int getAccountId() {
		return accountId;
	}

	public void setAccountId(int accountId) {
		this.accountId = accountId;
	}

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
		
}
