package bankaccount_chall;

import java.util.ArrayList;

public class Bank {
	private static ArrayList<BankAccount> accounts = new ArrayList<BankAccount>();
	
	public BankAccount findAccount(int id) {
		for(int i = 0; i < accounts.size(); i++) {
			if(accounts.get(i).getId() == id) {
				return accounts.get(i);
			}
		}
		return null;
	}
	public int findAccount(String name) {
		for(int i = 0; i < accounts.size(); i++) {
			if(accounts.get(i).getAccountHolder() == name) {
				return i;
			}
		}
		return (Integer) null;
	}
	
	public void addBankAccount(int password, String name, double balance) {
		BankAccount ba = new BankAccount(password, name, balance);
		accounts.add(ba);
	}
	
	public void removeBankAccount(int id) {
		for(int i = 0; i < accounts.size(); i++) {
			if(accounts.get(i).getId() == id) {
				System.out.println("Account with id: " + accounts.get(i).getId() + " removed!");
				accounts.remove(i);
			}
		}
	}
	
	public BankAccount verifyAccount(int id, int password) {
		for(int i = 0; i < accounts.size(); i++) {
			if(accounts.get(i).getId() == id) {
				if(accounts.get(i).getPassword() == password) {
					return accounts.get(i);
				} else {
					System.out.println("Incorrect password");
					return null;
				}
			} else {
				System.out.println("Account doesn't exist");
				return null;
			}
		}
		return null;
	}
	
	public void deposit(int id, double amount) {
		for(int i = 0; i < accounts.size(); i++) {
			if(accounts.get(i).getId() == id) {
				accounts.get(i).deposit(amount);
				accounts.get(i).addTransaction(accounts.get(i).getId(), 
						accounts.get(i).getAccountHolder(), amount, "Deposit");
			}
		}
	}
	
	public void withdraw(int id, double amount) {
		for(int i = 0; i < accounts.size(); i++) {
			if(accounts.get(i).getId() == id) {
				accounts.get(i).withdraw(amount);
				accounts.get(i).addTransaction(accounts.get(i).getId(), 
						accounts.get(i).getAccountHolder(), amount, "Withdraw");
			}
		}
	}
	
	public ArrayList<Transaction> getTransactionList(int id){
		for(int i = 0; i < accounts.size(); i++) {
			if(accounts.get(i).getId() == id) {
				return accounts.get(i).getTransactions();
			}
		}
		return null;
		
	}

	public ArrayList<BankAccount> getAccounts() {
		return accounts;
	}

	public void setAccounts(ArrayList<BankAccount> accounts) {
		this.accounts = accounts;
	}

	@Override
	public String toString() {
		return  getAccounts() + "\n";
	}
	
	public String printAcconts() {
		 
		StringBuilder stringBuilder = new StringBuilder();
		
		for (int i =0; i < this.accounts.size(); i++) {
			
			if(i == this.accounts.size()  ) {
				stringBuilder.append(Bank.accounts.get(i).getId() + "," + Bank.accounts.get(i).getAccountHolder() 
						+ "," + Bank.accounts.get(i).getBalance() + "," + Bank.accounts.get(i).getPassword());
			} else {
				stringBuilder.append(Bank.accounts.get(i).getId() + "," + Bank.accounts.get(i).getAccountHolder() 
						+ "," + Bank.accounts.get(i).getBalance() + "," + Bank.accounts.get(i).getPassword() + "\n");
			}
			
		}
		return stringBuilder.toString().trim(); 
	}
}
