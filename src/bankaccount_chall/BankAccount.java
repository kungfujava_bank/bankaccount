package bankaccount_chall;

import java.util.ArrayList;
import java.util.Date;

public class BankAccount {
	
	private int id;
	private int password;
	private String accountHolder;
	private double balance;
	private Date creationTimeStamp;
	private static int nextID = 1000000;
	
	private ArrayList<Transaction> transactions = new ArrayList<Transaction>();
	
	public BankAccount(int password, String accountHolder, double balance) {
		this.id = ++nextID;
		this.password = password;
		this.accountHolder = accountHolder;
		this.balance = balance;
		this.creationTimeStamp = new Date();
	}
	public BankAccount() {
	}
	
	public double deposit(double amount) {
		this.balance += amount;
		return balance;
	}
	
	public double withdraw(double amount){
		if(amount > balance){
			try {
				throw new ExcessOverdraw("Not enough money in account " + this.accountHolder);
			} catch (ExcessOverdraw e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			this.balance -= amount;
			
		}
		return balance;
		
	}
	
	public void addTransaction(int accountId, String accountName, double amount, String type) {
		Transaction t = new Transaction(accountId, accountName, amount, type);
		transactions.add(t);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getPassword() {
		return password;
	}

	public void setPassword(int password) {
		this.password = password;
	}

	public String getAccountHolder() {
		return accountHolder;
	}

	public void setAccountHolder(String accountHolder) {
		this.accountHolder = accountHolder;
	}

	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	public Date getCreationTimeStamp() {
		return creationTimeStamp;
	}

	public void setCreationTimeStamp(Date creationTimeStamp) {
		this.creationTimeStamp = creationTimeStamp;
	}
	
	public ArrayList<Transaction> getTransactions() {
		return transactions;
	}

	public void setTransactions(ArrayList<Transaction> transaction) {
		this.transactions = transaction;
	}

	@Override
	public String toString() {
		return id + "," + accountHolder + "," + balance 
				+ "," + password;
	}
}
