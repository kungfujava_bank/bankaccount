package bankaccount_chall;

import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

public class FileInputOutput extends BankAccount {
	
    public static void PrintToFile(String input){

        try {
            BufferedWriter outputWriter = new BufferedWriter(new FileWriter("C:/DemoIO/Accounts.txt", true));
            outputWriter.write(input);
            outputWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
    public static void PrintTransactionsToFile(String input){

        try {
            BufferedWriter outputWriter = new BufferedWriter(new FileWriter("C:/DemoIO/TransactionsLog.txt", true));
            outputWriter.write(input);
            outputWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
    public static ArrayList<BankAccount> RetrieveFromFile(String fileName){
    	ArrayList<BankAccount> temp = new ArrayList<BankAccount>();
        try {
            
            BufferedReader inputWriter = new BufferedReader((new FileReader(fileName)));

            int i = 0;
            String line;
            while((line = inputWriter.readLine()) != null){
                BankAccount bankAccountTemp = new BankAccount();
//                System.out.println("Line" + line);
                String[] splitString = line.split(",");
                bankAccountTemp.setId((Integer.parseInt(splitString[0])));
                bankAccountTemp.setAccountHolder(splitString[1]);
                bankAccountTemp.setBalance(Double.parseDouble(splitString[2]));
                bankAccountTemp.setPassword((Integer.parseInt(splitString[3])));
                temp.add(bankAccountTemp);
                

            }
            inputWriter.close();
//            System.out.println("Temp Size" + temp.size());
//            System.out.println(temp.get(1).toString());
            //System.out.println("Split  "  + splitString[0]);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        
        return temp;

    }
    public static void flushFile(String Filename){

        try {
            BufferedWriter outputWriter = new BufferedWriter(new FileWriter(Filename));
            outputWriter.write("");
            outputWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
    
    public static ArrayList<Transaction> RetrieveTransactions(String fileName){
    	ArrayList<Transaction> temp = new ArrayList<Transaction>();
        try {
            
            BufferedReader inputWriter = new BufferedReader((new FileReader(fileName)));

            int i = 0;
            String line;
            while((line = inputWriter.readLine()) != null){
                
                //System.out.println("Line" + line);
                String[] splitString = line.split(",");
                
                Transaction transaction = new Transaction(Integer.parseInt(splitString[0]),splitString[1], Double.parseDouble(splitString[2]), splitString[3] );
                //bankAccountTemp.setId((Integer.parseInt(splitString[0])));
                //bankAccountTemp.setAccountHolder(splitString[1]);
                //bankAccountTemp.setBalance(Double.parseDouble(splitString[2]));
                //bankAccountTemp.setPassword((Integer.parseInt(splitString[3])));
                temp.add(transaction);
                

            }
            inputWriter.close();
            //System.out.println("Split  "  + splitString[0]);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        
        return temp;

    }


}
